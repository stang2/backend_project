const mongoose = require('mongoose')
const { Schema } = mongoose
const eventSchema = Schema({
  name: String,
  content: String,
  class: String,
  startDate: Date,
  endDate: Date
},
{
  timestamp: true
})

module.exports = mongoose.model('Event', eventSchema)
