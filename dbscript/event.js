const mongoose = require('mongoose')
const Event = require('../models/Event')
mongoose.connect('mongodb://localhost:27017/Example')

async function clear () {
  await Event.deleteMany({})
}

async function main () {
  await clear()
  await Event.insertMany([
    {
      title: 'Title1', content: 'Content 1', startDate: new Date('2022-03-03 08:00'), endDate: new Date('2022-03-03 12:00'), class: 'a'
    },
    {
      title: 'Title2', content: 'Content 2', startDate: new Date('2022-03-20 08:00'), endDate: new Date('2022-03-20 12:00'), class: 'a'
    },
    {
      title: 'Title3', content: 'Content 3', startDate: new Date('2022-03-31 08:00'), endDate: new Date('2022-03-31 12:00'), class: 'c'
    },
    {
      title: 'Title4', content: 'Content 4', startDate: new Date('2022-03-31 13:00'), endDate: new Date('2022-03-31 16:00'), class: 'b'
    },
    {
      title: 'Title5', content: 'Content 5', startDate: new Date('2022-03-30 08:00'), endDate: new Date('2022-03-30 12:00'), class: 'c'
    }
  ])
  Event.find({})
}

main().then(function main () {
  console.log('Finish')
})
